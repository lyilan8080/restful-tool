package com.loser.plugin;

import com.loser.utils.JsonUtil;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;

@Component
public class MyRestConverterHandlerInPlugin extends RestConverterHandlerInPlugin {

    @Override
    public Object converter(Method method, String res) {
        System.out.println("user MyRestConverterHandlerInPlugin");
        if (Objects.isNull(res) || res.length() == 0) {
            return null;
        }
        try {
            if (method.getReturnType().equals(String.class)) {
                return res;
            }
            return JsonUtil.parse(res, method.getGenericReturnType());
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

    }
}
