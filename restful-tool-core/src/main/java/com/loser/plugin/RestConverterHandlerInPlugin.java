package com.loser.plugin;

import com.loser.core.support.base.BaseRequestFunction;
import com.loser.core.support.func.RestConverterHandler;
import com.loser.plugin.base.RestFulInPlugin;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * 结构装换器插件
 *
 * @author loser
 */
public abstract class RestConverterHandlerInPlugin implements RestFulInPlugin, RestConverterHandler {

    @Override
    public final Object invoke(MethodInvocation methodInvocation) throws Throwable {
        Object[] arguments = methodInvocation.getArguments();
        return converter((Method) arguments[0], (String) arguments[1]);
    }

    @Override
    public final Class<? extends BaseRequestFunction> getTargetType() {
        return RestConverterHandler.class;
    }

}
