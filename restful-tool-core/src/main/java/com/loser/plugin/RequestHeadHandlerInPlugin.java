package com.loser.plugin;

import com.loser.core.annotation.RestFulClient;
import com.loser.core.support.base.BaseRequestFunction;
import com.loser.core.support.func.RequestHeadHandler;
import com.loser.entity.ReqMethod;
import com.loser.plugin.base.RestFulInPlugin;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * 使用自定义类额外添加请求头信息（动态信息:比如token、sign）
 *
 * @author loser
 */
public abstract class RequestHeadHandlerInPlugin implements RestFulInPlugin, RequestHeadHandler {

    @Override
    public final Object invoke(MethodInvocation methodInvocation) throws Throwable {
        Object[] arguments = methodInvocation.getArguments();
        return doHead((RestFulClient) arguments[0], (ReqMethod) arguments[1], (Method) arguments[2], (Object[]) arguments[3]);
    }

    @Override
    public final Class<? extends BaseRequestFunction> getTargetType() {
        return RequestHeadHandler.class;
    }

}
